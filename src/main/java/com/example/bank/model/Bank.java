package com.example.bank.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Bank {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String address;
	@OneToMany(mappedBy = "bank")
	private List<Account> accounts;
	@OneToMany(mappedBy = "bank")
	private List<ATM> atms;
	
	public Bank() {
		super();
	}

	public Bank(Long id, String name, String address, List<Account> accounts, List<ATM> atms) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.accounts = accounts;
		this.atms = atms;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Account> accounts() {
		return accounts;
	}

	public void accounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public List<ATM> getAtms() {
		return atms;
	}

	public void setAtms(List<ATM> atms) {
		this.atms = atms;
	}
	
	
}
